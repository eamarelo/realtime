var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

io.on('connection', function (socket) {
	socket.on('test', function(test){
		console.log(test)
		socket.broadcast.emit('test2', test.text)
	})
  });

http.listen(3001, function(){
	console.log('listening on *:3000');
});